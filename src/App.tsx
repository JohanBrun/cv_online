import * as React from 'react';
import './styles/App.css';
import { BrowserRouter, Route, Link, Redirect } from "react-router-dom"

import { AppCV } from "./CV/AppCV"
import { ProjectsApp } from "./Projets/ProjectsApp"
import { ContactApp } from "./Contact/ContactApp"


export const App: React.FunctionComponent = (props) => {
  return (
    <div className="FullApp">
      <BrowserRouter>
        <div>
          <div className="Menu">
              <Link to='/CV' className="MenuItem">CV</Link>
              <Link to='/Projets' className="MenuItem">Projets</Link>
              <Link to='/Contact' className="MenuItem">Contact</Link>
          </div>

          <div className="main-route-place">
            <Route exact path="/">
              <Redirect to="/CV" />
            </Route>
            <Route path="/CV" component={AppCV} />
            <Route path="/Projets" component={ProjectsApp} />
            <Route path="/Contact" component= {ContactApp} />
          </div>
        </div>
      </BrowserRouter>
    </div>
  )
}
