import * as React from 'react';

interface IToggleButton {
    OnValue: string;
    OffValue: string;
    IsOn: boolean;
    setIsOn: any;
}

export const ToggleButton: React.FunctionComponent<IToggleButton> = (props) => {

    const [isOn, setIsOn] = React.useState(props.IsOn)
    let DisplayOnClassNameValue: string = ""
    let DisplayOffClassNameValue: string = ""

    if (isOn) {
        DisplayOnClassNameValue = "ToggleUnactive"
        DisplayOffClassNameValue = "ToggleActive"
    }
    else {
        DisplayOnClassNameValue = "ToggleActive"
        DisplayOffClassNameValue = "ToggleUnactive"
    }

    const SwitchToggle = (isOn: boolean) => {
        setIsOn(!isOn)
        props.setIsOn(!isOn)
    }

    return (
        <span className="Toggle">
            <div id="DisplayOn" className={DisplayOnClassNameValue} onClick={() => isOn ? "" : SwitchToggle(isOn) }>{props.OnValue}</div>
            <div id= "DisplayOff" className={DisplayOffClassNameValue} onClick={() => !isOn ? "" : SwitchToggle(isOn)}>{props.OffValue}</div>
        </span>
    )
}