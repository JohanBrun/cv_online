import * as React from "react";

export const SoftSkills: React.FunctionComponent = (props) => {

    let title: string = "Savoir-Être"
    let skillsArray: Array<string> = ["Curiosité", "Apprentissage", "Rigueur", "Exigence", "Communication", "Ecoute"]

    return(
        <div>
            <div className="subTitle">{title}</div>
            <ul className="softList">
                {
                    skillsArray.map(skill => {
                        return <li>{skill}</li>
                    })
                }
            </ul>
        </div>
    )
}