import * as React from "react";
import { ItSkillCard } from "./ItSkillCard";

export const ITSkills: React.FunctionComponent = (props) => {

    interface ITechnology {
        Name: string,
        Logo: string,
        Level: number
    }

    let javascriptTech: ITechnology = {
        Name : "Javascript",
        Logo : "Unofficial_JavaScript_logo_2.svg",
        Level : 4
    }

    let htmlTech: ITechnology = {
        Name : "HTML 5",
        Logo : "HTML5_logo_and_wordmark.svg",
        Level : 5
    }

    let cssTech: ITechnology = {
        Name : "CSS 3",
        Logo : "CSS3_logo_and_wordmark.svg",
        Level : 3
    }

    let pythonTech: ITechnology = {
        Name : "Python 3",
        Logo : "Python_logo_and_wordmark.svg",
        Level : 4
    }

    let djangoTech: ITechnology = {
        Name : "Django",
        Logo : "Django_logo.svg",
        Level : 4
    }

    let csharpTech: ITechnology = {
        Name : "C#",
        Logo : "C_Sharp_wordmark.svg",
        Level : 3
    }

    let dotNetCoreTech: ITechnology = {
        Name : ".Net core",
        Logo : "NET_Logo.svg",
        Level : 3
    }

    let vueTech: ITechnology = {
        Name : "Vue.js",
        Logo : "Vue.js_Logo_2.svg",
        Level : 2
    }

    let reactTech: ITechnology = {
        Name : "React",
        Logo : "React-icon.svg",
        Level : 4
    }

    let angularTech: ITechnology = {
        Name : "Angular",
        Logo : "Angular_full_color_logo.svg",
        Level : 3
    }

    let nodeTech: ITechnology = {
        Name : "Node.js",
        Logo : "Node.js_logo.svg",
        Level : 2
    }

    let typeScriptTech: ITechnology = {
        Name : "Typescript",
        Logo : "TypeScript_Logo.png",
        Level : 3
    }

    let gitTech : ITechnology = {
        Name : "Git",
        Logo : "Git-logo.svg",
        Level : 4
    }

    let techArray: Array<ITechnology> = [
            htmlTech, 
            cssTech, 
            javascriptTech, 
            pythonTech, 
            djangoTech,
            csharpTech,
            dotNetCoreTech,
            vueTech,
            reactTech,
            angularTech,
            nodeTech,
            typeScriptTech,
            gitTech]

    return(
        <div>
            <div className="subTitle">
                Compétences  en Informatique
            </div>
            <ul className="skillsGrid">
            {
                techArray.map(({Name, Logo, Level }) => (
                       <ItSkillCard name = {Name} img = {Logo} level = {Level}></ItSkillCard>
                ))
            }
            </ul>
        </div>
    )
}