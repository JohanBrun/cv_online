import * as React from "react";
import { ITSkills } from "./iTSkills";
import { LanguageSkills } from "./languageSkills";

export const HardSkills: React.FunctionComponent = (props) => {
    return(
        <div>
            <ITSkills></ITSkills>
            <LanguageSkills></LanguageSkills>
        </div>
    )
}