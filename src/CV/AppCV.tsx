import * as React from 'react';
import '../styles/App.css';

import { Title } from "./title"
import { Identity } from "./identity"
import { Skills } from "./skills"
import { Experiences } from "./experience"
import { Training } from "./training"

export const AppCV: React.FunctionComponent = (props) => {
  return (
    <div className="AppCV">
      <div className="title"><Title></Title></div>
      <div className="identity"><Identity></Identity></div>
      <div className="skills"><Skills></Skills></div>
      <div className="experience"><Experiences></Experiences></div>
      <div className="training"><Training></Training></div>

    </div>
  )
}
