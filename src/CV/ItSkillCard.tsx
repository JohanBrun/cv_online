import { Tooltip } from "@material-ui/core";
import * as React from "react";
import '../styles/App.css';

interface IItSkillCard {
    name: string,
    img: string,
    level: number
}

export const ItSkillCard: React.FunctionComponent<IItSkillCard> = (props) => {

    let grade: string = "Niveau : " + props.level + "/5";

    return(
        <>
            <Tooltip title={<span className="tooltip">{props.name} <br />{grade}</span>} arrow placement="bottom" enterTouchDelay={100}>
                <div>
                    <img src={require(`../assets/${props.img}`)} width="80" alt={props.name}></img>
                </div>
            </Tooltip>
        </>
    )
}