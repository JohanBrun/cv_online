import * as React from "react"
import { FaSearchMinus, FaSearchPlus } from "react-icons/fa"

interface ITraining {
    Year: string,
    SchoolName: string,
    Localisation: string,
    Diploma: string,
    Description: string,
    Important: boolean,
    DisplayDesc: boolean,
}

export const TrainingCard: React.FunctionComponent<ITraining> = (props) => {

    const [display, setDisplay] = React.useState(props.DisplayDesc)

    React.useEffect(() => {
        function renderDesc() {
          setDisplay(props.DisplayDesc)
        }
        renderDesc();
      }, [props.DisplayDesc]);

    const toggleSearch = (display: boolean) => {
        if (display) {
            return(
                <span className="lookDesc" onClick={() => setDisplay(!display)}><FaSearchMinus size={30}/></span>
            )
        }
        else {
            return <span className="lookDesc" onClick={() => setDisplay(!display)}><FaSearchPlus size={30}/></span>
        }
    }

    const showDesc = (display: boolean) => {
        if (display) {
            return (
                <div className="cardDesc">
                    {props.Description}
                </div>
            )
        }
        else {
            return (
                <div></div>
            )
        }
    }
    
    return(
        <div className="cardColor">
            <div className="card">
                <span className="cardItemYear">{props.Year} </span>
                <span className="cardItem"><b>{props.Diploma}</b> </span>
                <span className="cardItem"><i>{props.SchoolName}</i> </span>
                <span className="cardItem">{props.Localisation} </span>
                {toggleSearch(display)}
            </div>
            {showDesc(display)}
        </div>
    )
}