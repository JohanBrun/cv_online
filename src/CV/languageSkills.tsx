import * as React from "react";
import Flag from 'react-flagkit'

export const LanguageSkills: React.FunctionComponent = (props) => {
    return(
        <div>
            <div className="subTitle">Langues</div>
            <ul>
                    <div className="language"><Flag country="RE" /> Langue maternelle</div>
                    <div className="language"><Flag country="GB" /> Courant (TOEIC 975/990)</div>
            </ul>
        </div>
    )
}