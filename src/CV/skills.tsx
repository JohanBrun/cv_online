import * as React from "react";
import { HardSkills } from "./hardskills";
import { SoftSkills } from "./softskills";

export const Skills: React.FunctionComponent = (props) => {
    return(
        <div>
            <SoftSkills></SoftSkills>
            <HardSkills></HardSkills>
        </div>
    )
}