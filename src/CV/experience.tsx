import * as React from "react";
import { ToggleButton } from "../toggleButton";
import { XpCard } from "./xpCard";

export interface IExperience {
    StartDate : Date,
    EndDate : Date,
    JobName : string,
    Company : string,
    Localisation : string,
    Description : string,
    ItFocus : boolean
}

export const Experiences: React.FunctionComponent = (props) => {

    let avanadeXp : IExperience = {
        StartDate : new Date("2020-04-06"),
        EndDate : new Date("2020-09-30"),
        JobName : "Intern Full-Stack Dev Software Engineering",
        Company : "Avanade",
        Localisation : "Toulouse",
        Description : "Projet interne : développement du front d'une application de gestion de projet en react. / Projet externe : ajout de fonctionnalités et correction de bug sur une application de planification d'activité pour un client aéronautique. Technos utilisées Angular et .Net Core",
        ItFocus: true
    }

    let asaXp : IExperience = {
        StartDate: new Date('2017-05-02'),
        EndDate: new Date("2019-05-30"),
        JobName: "Chargé RH",
        Company: "Alliance Sages Adages",
        Localisation: "Toulouse",
        Description: "Prise en charge de l'ensemble des activités du service RH (administration du personnel, recrutement, formation, GPEC, conseil juridique à la direction)",
        ItFocus: false
    }

    let bbXp : IExperience = {
        StartDate: new Date('2017-02-01'),
        EndDate: new Date("2017-05-01"),
        JobName: "Chargé de recrutement",
        Company: "BugBusters",
        Localisation: "Toulouse",
        Description: "Recrutement de profils techniques pour des projets hardware informatiques",
        ItFocus: false
    }

    let harmonieXp : IExperience = {
        StartDate: new Date('2015-10-01'),
        EndDate: new Date("2016-09-30"),
        JobName: "Alternant Ressources Humaines",
        Company: "Harmonie Mutuelle",
        Localisation: "Albi",
        Description: "Gestion de l'administration du personnel, gestion des temps, formation, relations sociales, GPEC",
        ItFocus: false
    }

    let zodiacXp : IExperience = {
        StartDate: new Date('2014-09-01'),
        EndDate: new Date("2015-09-30"),
        JobName: "Alternant Ressources Humaines",
        Company: "Zodiac Aerospace",
        Localisation: "La Teste de Buch",
        Description: "Gestion des temps, formation, relation OCPA, relations sociales, conseil juridique",
        ItFocus: false
    }

    let mcDoXp : IExperience = {
        StartDate: new Date('2014-06-01'),
        EndDate: new Date("2014-07-31"),
        JobName: "Equipier polyvalent",
        Company: "McDonald's",
        Localisation: "Pau",
        Description: "Travail en cuisine",
        ItFocus: false
    }

    let prefXp : IExperience = {
        StartDate: new Date('2014-02-01'),
        EndDate: new Date("2014-04-30"),
        JobName: "Vacataire Assistant Elections",
        Company: "Prefecture de Gironde",
        Localisation: "Bordeaux",
        Description: "Contrôle juridique des dossiers de candidature aux élections municipales",
        ItFocus: false
    }

    let rexamXp : IExperience = {
        StartDate: new Date('2013-07-01'),
        EndDate: new Date("2013-08-31"),
        JobName: "Stagiaire Ressources Humaines",
        Company: "Rexam Beverage Can",
        Localisation: "Pau",
        Description: "Support administratif de la responsable RH",
        ItFocus: false
    }

    let coloXp : IExperience = {
        StartDate: new Date('2012-07-01'),
        EndDate: new Date("2012-08-31"),
        JobName: "Animateur Colonie de Vacances",
        Company: "Vacances pour tous",
        Localisation: "Saint Jean de Luz",
        Description: "Organiser et superviser les activités des enfants sur le camp",
        ItFocus: false
    }

    let pruneauXp : IExperience = {
        StartDate: new Date('2010-08-01'),
        EndDate: new Date("2010-08-31"),
        JobName: "Ouvrier agricole",
        Company: "Coopérative Pruneaux d'Agen",
        Localisation: "Villeneuve sur Lot",
        Description: "Déclayage des pruneaux",
        ItFocus: false
    }

    let XpList: Array<IExperience> = [
        avanadeXp,
        asaXp,
        bbXp,
        harmonieXp,
        zodiacXp,
        mcDoXp,
        prefXp,
        rexamXp,
        coloXp,
        pruneauXp,
    ]

    const [isOn, setIsOn] = React.useState(true)

    const listToDisplay = (isOn : boolean) => {
        if (isOn) {
            return (
                <>
                {XpList.filter((xp) => xp.ItFocus === true).map(({StartDate, EndDate, JobName, Company, Localisation, Description, ItFocus}) => (
                    <XpCard 
                        StartDate={StartDate} 
                        EndDate={EndDate} 
                        JobName={JobName} 
                        Company={Company} 
                        Localisation={Localisation} 
                        Description={Description} 
                        ItFocus={ItFocus}
                        DisplayDesc={isOn}>
                    </XpCard>
                ))}
                </>
            )}
        else {
            return (
                <>
                {XpList.map(({StartDate, EndDate, JobName, Company, Localisation, Description, ItFocus}) => (
                    <XpCard 
                        StartDate={StartDate} 
                        EndDate={EndDate} 
                        JobName={JobName} 
                        Company={Company} 
                        Localisation={Localisation} 
                        Description={Description} 
                        ItFocus={ItFocus}
                        DisplayDesc={isOn}>
                    </XpCard>
                    ))
                }
                </>
            )}}


    return(
        <div className="cardsList">
            <div className="subTitle">
                <span>Expériences</span>
                <span><ToggleButton OnValue="IT" OffValue="Tout" IsOn={isOn} setIsOn={setIsOn}></ToggleButton></span>
            </div>
            <ul>
                {listToDisplay(isOn)}
            </ul>
        </div>
    )
}