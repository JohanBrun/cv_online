import * as React from "react";
import { ToggleButton } from "../toggleButton";
import { TrainingCard } from "./trainingCard";

export const Training: React.FunctionComponent = (props) => {
     
    interface ITraining {
        Year: string,
        SchoolName: string,
        Localisation: string,
        Diploma: string,
        Description: string
        Important: boolean
    }

    let dafTraining: ITraining = {
        Year: "2020",
        SchoolName : "INP - ENSEEIHT",
        Localisation : "Toulouse",
        Diploma: "DU Développeur d'Application Fullstack",
        Description: "Algorythmie : Python, Javascript / Backend : Node.js, Django / Frontend : React, Vue.js / Git, Linux, Bash, Agile",
        Important: true
    }

    let m2RhTraining: ITraining = {
        Year: "2016",
        SchoolName: "IGS",
        Localisation: "Toulouse",
        Diploma: "Master II Responsable en Management et Direction des Ressources Humaines",
        Description: "Management, gestion de projet, GPEC, ingénierie de formation, négociation, droit social",
        Important: true
    }

    let m1RhTraining: ITraining = {
        Year: "2015",
        SchoolName: "CESI",
        Localisation: "Bordeaux",
        Diploma: "Master I Management des Ressources Humaines en Entreprise",
        Description: "Administration du personnel, gestion des temps, formation, relations sociales",
        Important: false
    }

    let droitTraining: ITraining = {
        Year: "2013",
        SchoolName: "Université Montesquieu Bordeaux IV",
        Localisation: "Bordeaux",
        Diploma: "Licence en Droit et Sciences Politique",
        Description: "Droit privé : droit civil, droit des contrats, droit pénal, droit du travail / Droit public : droit constitutionnel, droit administratif, droit des libertés fondamentales, droit européen",
        Important: false,
    }

    let bacTraining: ITraining = {
        Year: "2009",
        SchoolName: "Lycée Louis Barthou",
        Localisation: "Pau", 
        Diploma: "Baccalauréat Scientifique",
        Description: "Mention AB",
        Important: true
    }

    let trainingList: Array<ITraining> = [
        dafTraining,
        m2RhTraining,
        m1RhTraining,
        droitTraining,
        bacTraining
    ]

    const [isOn, setIsOn] = React.useState(true)

    const listToDisplay = (isOn : boolean) => {
        if (isOn) {
            return (
                <>
                {
                  trainingList.filter((xp) => xp.Important === true).map(({Year, SchoolName, Localisation, Diploma, Description, Important}) => (
                    <TrainingCard 
                        Year={Year} 
                        SchoolName={SchoolName} 
                        Localisation={Localisation} 
                        Diploma={Diploma} 
                        Description={Description} 
                        Important={Important}
                        DisplayDesc={isOn}>
                    </TrainingCard>
                  ))  
                }
                </>
            )}
        else {
            return (
                <>
                {
                  trainingList.map(({Year, SchoolName, Localisation, Diploma, Description, Important}) => (
                    <TrainingCard 
                        Year={Year} 
                        SchoolName={SchoolName} 
                        Localisation={Localisation} 
                        Diploma={Diploma} 
                        Description={Description} 
                        Important={Important}
                        DisplayDesc={isOn}>
                    </TrainingCard>
                  ))  
                }
                </>
            )}}

    return(
        <div className="cardsList">
            <div className="subTitle">
                <span>Etudes</span>
                <span><ToggleButton OnValue="Majeures" OffValue="Tout" IsOn={isOn} setIsOn={setIsOn}></ToggleButton></span>
            </div>
            <ul>
                {listToDisplay(isOn)}
            </ul>
        </div>
    )
}