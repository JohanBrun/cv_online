import * as React from "react";
import '../styles/App.css';
import { FaPhoneSquare, FaTelegramPlane, FaCity, FaUserAlt } from 'react-icons/fa'

export const Identity: React.FunctionComponent = (props) => {
    
    let firstName: string = "Johan"
    let lastName: string = "Brun"
    let phoneNumber: string = "+33.6.59.07.07.54"
    let mail: string = "johan.brun2@gmail.com"
    let address : string = "Toulouse"
    let bio : string = "Après une reconversion professionnelle, je me consacre désormais\
                         au développement web et suis disponible pour tout projet sur la région\
                         toulousaine"
    
    return(
        <div>
            <span className="name">{firstName} {lastName}</span> <br />
            <div className="idText"><span className="icon"> <FaPhoneSquare size={40} /> </span> <span className="idItem">:</span> {phoneNumber} </div>
            <div className="idText"><span className="icon"> <FaTelegramPlane size={40} /> </span> <span className="idItem">:</span> <a href={`mailto:${mail}?subject="Contact CV"`}>{mail}</a> </div>
            <div className="idText"><span className="icon"><FaCity size={40}/> </span> <span className="idItem">:</span> {address} </div>
            <div className="idText"><span className="icon"><FaUserAlt size={40}/> </span><span> <span className="idItem">:</span> </span> <span>{bio}</span></div>
            <br />
            <br />
        </div>
    )
}