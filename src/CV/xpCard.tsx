import * as React from "react";
import { FaArrowRight, FaSearchMinus, FaSearchPlus } from 'react-icons/fa'

interface IExperience {
    StartDate : Date,
    EndDate : Date,
    JobName : string,
    Company : string,
    Localisation : string,
    Description : string,
    ItFocus : boolean
    DisplayDesc : boolean
}


export const XpCard: React.FunctionComponent<IExperience> = (props) => {

    const [display, setDisplay] = React.useState(props.DisplayDesc)

    React.useEffect(() => {
        function renderDesc() {
          setDisplay(props.DisplayDesc)
        }
        renderDesc();
      }, [props.DisplayDesc]);

    const toggleSearch = (display: boolean) => {
        if (display) {
            return(
                <span className="lookDesc" onClick={() => setDisplay(!display)}><FaSearchMinus size={30}/></span>
            )
        }
        else {
            return <span className="lookDesc" onClick={() => setDisplay(!display)}><FaSearchPlus size={30}/></span>
        }
    }

    const showDesc = (display: boolean) => {
        if (display) {
            return (
                <div className="cardDesc">
                    {props.Description}
                </div>
            )
        }
        else {
            return (
                <div></div>
            )
        }
    }

    return(
        <div className="cardColor">
            <div className="card">
                <span className="cardItemShort">{props.StartDate.toLocaleString('default', { month: 'short' })} {props.StartDate.getFullYear()}</span>
                <span className="cardItemFixed"><FaArrowRight/></span>
                <span className="cardItemShort">{props.EndDate.toLocaleString('default', { month: 'short' })} {props.EndDate.getFullYear()}</span>
                <span className="cardItem"><b>{props.JobName}</b></span>
                <span className="cardItem"><i>{props.Company}</i></span>
                <span className="cardItem">{props.Localisation}</span>
                {toggleSearch(display)}
            </div>
            {showDesc(display)}
        </div>
        
    )
}