import * as React from 'react';
import { Redirect } from 'react-router-dom';
import "../styles/Contact.css"

export const ContactApp: React.FunctionComponent = (props) => {


    let mailSent = () => {
        alert("Message envoyé !")
    }

    return(
        <div>
            <h1 className="ContactTitle">Vous êtes intéressez par mon profil ? N'hésitez pas, contactez moi !</h1>
            <div className="ContactApp">
                <form target="_blank" action="https://formsubmit.co/johan.brun2@gmail.com" method="POST">
                    <div className="form-group">
                        <div className="form-row">
                            <div className="form-col">
                                <input type="text" name="name" className="form-control" placeholder="Nom Prénom*" required />
                            </div>
                            <div className="form-col">
                                <input type="email" name="email" className="form-control" placeholder="Email*" required />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <textarea placeholder=" Votre message*" className="form-control" name="message" rows={10} cols={100} required></textarea>
                    </div>
                    <button type="submit" className="btn btn-lg btn-dark btn-block" onClick={() => mailSent()} >Envoyer</button>
                </form>
                <div className="OtherContacts">
                    <div className="textLG">Ou retrouvez moi sur LinkedIn ou GitLab :</div>
                    <div className="LinkLab">
                        <a href="https://www.linkedin.com/in/johan-brun/"><img className="LinkLabItem" src={require("../assets/LinkedIn_Logo.svg")} height={50} alt="LinkedIn"></img></a>
                        <a href="https://gitlab.com/JohanBrun"><img className="LinkLabItem" src={require("../assets/GitLab_Logo.svg")} height={50} alt="GitLab"></img></a>
                    </div>
                </div>
            </div>
        </div>
    )
}